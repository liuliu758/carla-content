default: dist

dist:
	@git archive -v --format tar.gz HEAD -o `git log --pretty=format:"%h" -n 1`.tar.gz

check:
	@echo "Looking for Non-ASCII characters in file names"
	@LC_ALL=C find . -name '*[! -~]*' > non-ascii.txt
	@cat non-ascii.txt
	@test ! -s non-ascii.txt
	@rm -f non-ascii.txt
	@echo "OK"
